#!/usr/bin/python
import threading
import urllib.parse
from logging.handlers import RotatingFileHandler
import os
import random
import telebot.apihelper
import operator
import configparser
from telebot import types
from MessageHandler import MessageHandler
from DatabaseHandler import DatabaseHandler
import logging
import schedule
import time
import requests


LOG_ENABLED = True


class BotConfig:
    def __init__(self):
        cfg = configparser.ConfigParser()
        cfg.read(self.abs_path("/config.ini"))
        self.apitoken = cfg["TelegramBot"]["apitoken"]
        self.shared_group = int(cfg["TelegramBot"]["shared_group_id"])
        self.db_name = cfg["TelegramBot"]["databaseName"]
        self.game_counter_enabled = bool(cfg["GameCounter"]["post_to_game_counter"])
        self.game_counter_url = cfg["GameCounter"]["game_counter_url"]
        self.game_counter_token = cfg["GameCounter"]["games_counter_api_key"]

    def abs_path(self, file):
        script_path = os.path.abspath(__file__)
        return script_path[:script_path.rfind('/')] + file


config = BotConfig()
# Inicialización del bot
bot = telebot.TeleBot(config.apitoken)

if LOG_ENABLED:
    # Rotate log every MB up to 3 times
    handler = RotatingFileHandler("salonrecreativolog.txt", maxBytes=1024*1024, backupCount=2)
    FORMAT = "%(asctime)-15s %(message)s"
    logger = logging.getLogger("mariano")
    handler.setFormatter(logging.Formatter(FORMAT))
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    logging.getLogger("requests").setLevel(logging.WARNING)


# Sí se quieren ver puntuaciones
@bot.message_handler(commands=['top'])
def send_top(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            last_number = msg.db.get_last_round_number()
            lista = msg.db.get_recent_games_list(last_number - 5)

            # devolver menu con los nombres
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, selective=True)
            for game in lista:
                markup.add(f"/de {game}")
            msg.send_private_message('Selecciona el juego para ver los puntos', reply_markup=markup)


@bot.message_handler(commands=['topanteriores'])
def send_top_all(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            lista = msg.db.get_games_list()

            # devolver menu con los nombres
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, selective=True)
            for game in lista:
                markup.add(f"/de {game}")
            msg.send_private_message('Selecciona el juego para ver los puntos', reply_markup=markup)


@bot.message_handler(commands=['de'])
def send_top_juego(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            juego = " ".join(msg.parameters)
            if len(juego) > 0:
                markup = types.ReplyKeyboardRemove(selective=True)
                game_id = msg.db.get_game_id(juego)
                if game_id is None:
                    msg.send_private_message("juego erroneo", reply_markup=markup)
                    return
                top = msg.db.get_top(game_id)
                if len(top) > 0:
                    top_message = generate_top(top, msg.db)
                    msg.send_private_message(top_message, parse_mode="MarkdownV2", reply_markup=markup)
                else:
                    msg.send_private_message("No hay puntajes registrados", reply_markup=markup)
            else:
                msg.send_private_message("comando erroneo")


@bot.message_handler(commands=['en'])
def send_puntos(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            close_keyboard = types.ReplyKeyboardRemove(selective=True)
            if len(msg.parameters) < 3:
                msg.send_private_message("comando erroneo")
                return
            try:
                puntos = int(msg.parameters[-2])
                if puntos <= 0:
                    msg.send_private_message("Puntos erróneos", reply_markup=close_keyboard)
                    return

            except ValueError:
                msg.send_private_message("Puntos erróneos", reply_markup=close_keyboard)
                return
            juego = " ".join(msg.parameters[:-2])

            if msg.db.get_player_name(msg.user_id) is None:
                msg.send_private_message("Debe definir el nombre con el comando /nombre {nombre}", reply_markup=close_keyboard)
                return
            game = msg.db.get_game_id(juego)
            if game is None:
                msg.send_private_message("Juego erróneo", reply_markup=close_keyboard)
                return
            msg.db.save_score(game, msg.user_id, puntos)
            save_credits(msg.db, game, msg.user_id)

            msg.send_private_message("Puntuación añadida", reply_markup=close_keyboard)
            if not msg.in_shared_group(send_message=False):
                user, name = msg.db.get_player_name(msg.user_id)
                msg.send_shared_message(f"{user} ({name}) hizo {puntos} puntos en {juego}")


@bot.message_handler(commands=['puntos'])
def store_points(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            generate_points_game_selection(msg, False)


@bot.message_handler(commands=['puntosanteriores'])
def store_points(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            generate_points_game_selection(msg, True)


@bot.message_handler(commands=['nombre'])
def set_name(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.in_shared_group():
            if len(msg.parameters) != 1 or len(msg.parameters[0].strip()) != 3:
                msg.send_private_message("Debe indicar un nombre de tres letras")
                return
            else:
                name = msg.parameters[0]
                first_name = message.from_user.first_name
                last_name = message.from_user.last_name
                if not msg.db.player_name_exists(name, msg.user_id):
                    update_player_name(msg.db, msg.user_id, name, first_name, last_name)
                    msg.send_private_message("Nombre registrado")
                else:
                    msg.send_private_message(f"El nombre {name} ya está usado")


@bot.message_handler(commands=['proponer'])
def propose_game(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            game = " ".join(msg.parameters)
            if len(game) == 0:
                msg.send_private_message('Debe indicar el juego')
            else:
                creditos = msg.db.get_unused_player_credits(msg.user_id)
                if len(creditos) < 1:
                    msg.send_private_message('No tiene créditos suficientes ')
                else:
                    msg.db.save_proposed_game(msg.user_id, game)
                    msg.db.use_player_credit(msg.user_id, creditos[0][0])
                    msg.send_private_message("Juego propuesto guardado")
                    if not msg.in_shared_group(send_message=False):
                        user, name = msg.db.get_player_name(msg.user_id)
                        msg.send_shared_message(f"{user} ({name}) propuso {game}")


@bot.message_handler(commands=['propuestos'])
def list_proposed_games(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            games = msg.db.get_proposed_games_votes()

            if len(games) == 0:
                text = "No hay juegos propuestos"
            else:
                text = "Juego propuesto: Votos (probabilidad)\n\n"
                total = 0
                for game in games:
                    total += game[1]
                for game in games:
                    text += f"{game[0]}: {game[1]} ({round(game[1]/total*100, 2)}%)\n"
                text += f"\nTotal de votos: {total}"
            count = len(msg.db.get_unused_player_credits(msg.user_id))
            text += f"\nTienes {count} créditos disponibles."
            msg.send_private_message(text)


@bot.message_handler(commands=['creditos'])
def list_proposed_games(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            count = len(msg.db.get_unused_player_credits(msg.user_id))
            text = f"\nTienes {count} créditos disponibles."
            msg.send_private_message(text)


def post_game_name_to_game_counter(game_name, config):
    if config.game_counter_enabled:
        encoded_name = urllib.parse.quote(game_name)
        url = config.game_counter_url.format(config.game_counter_token, encoded_name)
        params = {"game": encoded_name, "token": config.game_counter_token}
        response = requests.get(url, params)
        if response.ok:
            logger.info("Selected game posted to game counter: [%s]", encoded_name)
        else:
            logger.error("Failed to post game name to game counter: status code: [%s] response:[%s] ", response.status_code, response)


@bot.message_handler(commands=['sortear'])
def select_proposed_game(message):
    game = ""
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user() and msg.in_shared_group():
            games = msg.db.get_proposed_games()

            if len(games) == 0:
                text = "No hay juegos propuestos"
            else:
                game, text = select_random_game(games)
                round_number = msg.db.get_last_round_number() + 1
                msg.db.create_round(round_number, game)
                msg.db.remove_proposed_games(game)
                text += "\nNueva ronda generada"

            msg.send_shared_message(text)
    if game != "":
        post_game_name_to_game_counter(game, config)

@bot.message_handler(commands=['inicial'])
def initial_round(message):

    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user() and msg.in_shared_group():
            last_round = msg.db.get_last_round_number()
            if last_round != 0:
                msg.send_private_message('Sólo se puede definir la ronda inicial')
            else:
                text = message.text
                game = text[8:].strip()
                if len(game) == 0:
                    msg.send_private_message('Debe indicar el juego')
                else:
                    msg.db.create_round(1, game)
                    msg.send_private_message('Ronda inicial generada')


@bot.message_handler(commands=['cambio'])
def change(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            with open(config.abs_path('/pesetas/pesetas1.png'), 'rb') as photo:
                bot.send_photo(message.chat.id, photo, reply_to_message_id=message.message_id)


@bot.message_handler(commands=['fichas'])
def change(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            with open(config.abs_path('/fichas/fichas.jpg'), 'rb') as photo:
                bot.send_photo(message.chat.id, photo, reply_to_message_id=message.message_id)


@bot.message_handler(commands=['actual'])
def current_round(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            last_number = msg.db.get_last_round_number()
            if last_number == 0:
                msg.send_private_message("No se estableció la ronda inicial")
            else:
                current = msg.db.get_round(last_number)
                text = f"Ronda Nro: {current[0]}\nJuego: {current[1]}"
                msg.send_private_message(text)


@bot.message_handler(commands=['jugados'])
def current_round(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            last_number = msg.db.get_last_round_number()
            if last_number == 0:
                msg.send_private_message("No se estableció la ronda inicial")
            else:
                rounds = msg.db.list_previous_rounds(last_number)
                text = "Rondas anteriores: \n"
                for r in rounds:
                    text += f"{r[0]:<2} - {r[1]}\n"
                msg.send_private_message(text)


@bot.message_handler(commands=['whois'])
def list_players(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            players = msg.db.list_players()
            text = "Jugadores registrados:\n"
            for player in players:
                text += f"{player[0]:>3} - {player[1]}\n"
            msg.send_private_message(text)


@bot.message_handler(commands=['mispropuestos'])
def my_proposed(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            proposed = msg.db.get_proposed_games_of_user(msg.user_id)
            if len(proposed) == 0:
                msg.send_private_message("No hay juegos propuestos")
            else:
                name = msg.db.get_player_name(msg.user_id)
                response = f"Juegos propuestos por {name[0]} ({name[1]}):\n\n"
                for game in proposed:
                    response += f"{game[1]}\n"
                msg.send_private_message(response)


@bot.message_handler(commands=['cambiar'])
def change_proposed(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            proposed = msg.db.get_proposed_games_of_user(msg.user_id)

            if "/por" not in msg.parameters:
                new_game = " ".join(msg.parameters)
                if len(new_game.strip()) == 0:
                    msg.send_private_message("Debe indicar el nuevo juego a proponer")
                    return
                markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, selective=True)
                for game in proposed:
                    markup.add(f"/cambiar {game[1]} /por {new_game} ")
                msg.send_private_message("Seleccione el juego propuesto a cambiar: ", reply_markup=markup)
            else:
                close_keyboard = types.ReplyKeyboardRemove(selective=True)
                ix = msg.parameters.index("/por")
                old_name = " ".join(msg.parameters[:ix])
                new_name = " ".join(msg.parameters[ix+1:])
                for game in proposed:
                    if game[1] == old_name:
                        msg.db.update_proposed_game(game[0], new_name)
                        msg.send_private_message("Juego propuesto cambiado", reply_markup=close_keyboard)
                        return
                msg.send_private_message("No se encontró el juego seleccionado", reply_markup=close_keyboard)


@bot.message_handler(commands=['deshacer'])
def change_proposed(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user():
            game = " ".join(msg.parameters)
            if len(game) == 0 or len(game.strip()) == 0:
                # devolver menu con los nombres
                lista = msg.db.get_games_list()
                markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, selective=True)
                for game in lista:
                    markup.add(f"/deshacer {game}")
                msg.send_private_message('Selecciona el juego para deshacer el último puntaje', reply_markup=markup)
            else:
                close_keyboard = types.ReplyKeyboardRemove(selective=True)
                game_id = msg.db.get_game_id(game)
                if game_id is None:
                    msg.send_private_message("No se encontró el juego seleccionado", reply_markup=close_keyboard)
                    return
                else:
                    scores = msg.db.get_player_scores(msg.user_id, game_id)
                    if len(scores) == 0:
                        msg.send_private_message("No se encontró un puntaje para el juego seleccionado", reply_markup=close_keyboard)
                        return
                    else:
                        score_to_remove = scores[0]
                        msg.db.remove_score(game_id, msg.user_id, score_to_remove[0])
                        msg.send_private_message(f"Eliminado puntaje {score_to_remove[1]} en {game}", reply_markup=close_keyboard)
                        return


@bot.message_handler(content_types=['new_chat_members'])
def welcome_message(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.in_shared_group():
            logger.info(message.new_chat_members[0])
            if message.new_chat_members[0].username is not None:
                username = message.new_chat_members[0].username
            else:
                username = message.new_chat_members[0].first_name
            text = f"Bienvenido @{username} al salón recreativo de la  ACHUS. Las reglas están en la" \
                   " descripción del grupo y los comandos de Mariano se pueden consultar con /help. " \
                   "Para poder participar lo primero es registrar un nombre de 3 letras con el comando /nombre. " \
                   "Cualquier duda pregunta por aquí.\n"
            last_number = msg.db.get_last_round_number()
            if last_number > 0:
                current = msg.db.get_round(last_number)
                text += f"\nEsta semana estamos jugando a: {current[1]}\n"
            msg.send_private_message(text)


@bot.message_handler(commands=['help'])
def process_help(message):
    with MessageHandler(bot, message, config) as msg:
        if msg.valid_user() or msg.in_shared_group(send_message=False):
            help_message = "Comandos: \n" \
                           "/actual : Muestra información de la ronda actual\n" \
                           "/cambiar {nuevo_nombre} : Cambia el nombre de un juego propuesto por otro\n" \
                           "/cambio: Cambio para el arcade\n" \
                           "/creditos: Muestra la cantidad de créditos disponibles para proponer\n" \
                           "/deshacer: Deshace el último puntaje del juego seleccionado\n" \
                           "/fichas: Fichas para el arcade\n" \
                           "/jugados: Muestra información de rondas anteriores \n" \
                           "/mispropuestos: Muestra los juegos propuestos por usuario\n" \
                           "/nombre {nombre} : Establece el nombre a usar en el ranking\n" \
                           "/puntos {numero} : Introduce puntos en uno de los últimos 5 juegos\n" \
                           "/puntosanteriores {numero} : Introduce puntos en un juego\n" \
                           "/propuestos : Lista los juegos propuestos\n" \
                           "/proponer {juego} : Agrega un juego a la lista de propuestos\n" \
                           "/top : Muestra el top de puntajes de uno de los últimos 5 juegos\n" \
                           "/topanteriores : Muestra el top de puntajes de un juego\n" \
                           "/whois : Muestra los nombres de los participantes\n"
            msg.send_private_message(help_message)


@bot.message_handler(func=lambda message: True)
def echo_message(message):
    print("Mensaje no procesado: ", message.text)


def generate_current_round_top(encabezado):
    with DatabaseHandler(config.db_name) as db:
        actual = db.get_last_round_number()
        top = db.get_top(actual)
        if len(top) > 0:
            top_message = generate_top(top, db, encabezado)
            bot.send_message(config.shared_group, top_message, parse_mode="MarkdownV2")
        else:
            bot.send_message(config.shared_group, "No hay puntajes registrados")


def generate_points_game_selection(msg, show_all_games):
    # comprobamos que los puntos son enteros:
    try:
        puntos = msg.parameters[0]
        puntos = int(puntos)
        if puntos <= 0:
            msg.send_private_message('Puntos incorrectos')
            return
    except IndexError:
        msg.send_private_message('Debe indicar el puntaje')
        return

    except ValueError:
        msg.send_private_message('Puntos incorrectos')
        return

    # llenar lista nombres arcade del chat
    if show_all_games:
        lista = msg.db.get_games_list()
    else:
        last_number = msg.db.get_last_round_number()
        lista = msg.db.get_recent_games_list(last_number - 5)

    # devolver menu con los nombres
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, selective=True)
    for game in lista:
        markup.add(f"/en {game} {puntos} puntos")
    msg.send_private_message('Selecciona el juego donde introducir los puntos', reply_markup=markup)


def count_positions(top):
    count = 0
    prev_score = -1
    for number, entry in enumerate(top):
        score = entry[1]
        if prev_score != score:
            count += 1
        prev_score = score
    return count


def generate_top(top, db, header=""):
    temp = "```\n"
    temp += header
    top_added = False
    teams = {}
    positions_count = count_positions(top)
    max_points = len(top)
    max_score_length = len(str(top[0][1]))
    max_score_length = max_score_length if max_score_length <= 10 else 10
    template = "{:>2}p :{:>3} -> {:>" + str(max_score_length) + "} {}\n"
    prev_score=-1
    position = -1
    for number, entry in enumerate(top):
        score = entry[1]
        if prev_score != score:
            points = max_points - number
            position += 1
            if positions_count > 6:
                if (position == 5 or number > 5) and not top_added:
                    temp += "^^^^^^^  Top 5  ^^^^^^^\n\n"
                    top_added = True
                if position == positions_count-1:
                    temp += "\n--- Necesita Mejorar ---\n"
        icon = "  "
        if entry[2] is not None:
            icon = entry[2]
            if icon in teams:
                teams[icon].append(points)
            else:
                teams[icon] = [points]
        temp += template.format(points, entry[0], score, icon)
        prev_score = score
    temp += "\nPuntaje por equipos:\n\n"
    temp += get_team_score(teams, db)
    temp += "```"
    return temp


def get_team_score(teams_points, db):

    if len(teams_points) < db.get_team_count():
        return "Hay equipos sin jugar"

    # Get the count of players on the team with least players
    count = None
    for team in teams_points:
        length = len(teams_points[team])
        if count is None or count > length:
            count = length
    
    team_score = {}
    for team in teams_points:
        points = sum(teams_points[team][:count])
        team_score[team] = points
    


    template = "{:<2} -> {:<} pts\n"
    sorted_teams = sorted(team_score.items(), key=operator.itemgetter(1), reverse=True)    
    top = ""	   
    for entry in sorted_teams:
        top += template.format(entry[0], entry[1])    

    return top
    


def update_player_name(db, user_id, user_name, first_name, last_name):
    name = db.get_player_name(user_id)
    if name is None:
        db.create_player(user_id, user_name, first_name, last_name)
    elif name[0] != user_name:
        db.update_player_name(user_id, user_name)


def save_credits(db, round_id, user_id):
    if db.get_player_game_credits(user_id, round_id) == 0:
        last_round = db.get_last_round_number()
        if round_id == last_round:
            db.create_player_credit(user_id, round_id)


def select_random_game(games):
    selected_game_name = random.choice(games)[1]
    text = f"Juego Seleccionado: {selected_game_name}\n"
    text += "Propuesto por:\n"
    names = set()
    for name, game in games:
        if game == selected_game_name:
            names.add(name)
    for name in names:
        text += f"{name}\n"
    return selected_game_name, text


if __name__ == '__main__':
    telebot.apihelper.READ_TIMEOUT = 5
    schedule.every().day.at("08:00").do(generate_current_round_top, encabezado="Clasificación diaria:\n")
    schedule.every().sunday.at("20:00").do(generate_current_round_top, encabezado="Clasificación Final:\n")
    # schedule.every().minute.do(generate_current_round_top, encabezado="Clasificación diaria:\n")
    # schedule.every(5).minutes.do(generate_current_round_top, encabezado="Clasificación Final:\n")

    threading.Thread(target=bot.infinity_polling, name='bot_infinity_polling', daemon=True).start()
    while True:
        schedule.run_pending()
        time.sleep(1)
