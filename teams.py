from DatabaseHandler import DatabaseHandler


def create_teams(chat_id, teams):
    with DatabaseHandler(chat_id) as db:
        for team in teams:
            name = team[0]
            icon = team[1]
            team = db.get_team_by_name(name)
            if team is not None:
                print(f"Team {name} already exists")
            else:
                db.add_team(name, icon)
                print("Added team {name}")


def get_member(existing_members, player_id):
    for member in existing_members:
        if member[1] == player_id:
            existing_members.remove(member)
            return member
    return None


def create_members(chat_id, team_name, members):
    with DatabaseHandler(chat_id) as db:
        team = db.get_team_by_name(team_name)
        team_id = team[0]
        existing_members = db.get_existing_team_members(team_id)
        for member in members:
            player_id = db.get_player_id_by_username(member)
            if player_id is None:
                print(f"Player not found {member}")
            else:
                member_db = get_member(existing_members, player_id)
                if member_db is not None:
                    print(f"Team member {member} already added")
                else:
                    db.add_team_member(team_id, player_id)
                    print(f"Added {member} to team {team_name}")
        for member in existing_members:
            print(f"Removing team member {member[2]}")
            db.remove_team_member(team_id, member[1])


if __name__ == '__main__':
    import_chat_id = -1001371988682
    teams_data = [
        ["The Paquetes", "📦"],
        ["Wiz Panic", "🧙‍"],
        ["Los Manquificos", "🥊"],
        ["Komando kemando el mando", "🕹"]

    ]
    create_teams(import_chat_id, teams_data)
    team_members = ["DAN", "SID", "RAM", "JVS", "ZID", "MSH", "KDZ", "NON"]
    create_members(import_chat_id, "The Paquetes", team_members)

    team_members = ["CPC", "RAU", "KAL", "CRK", "MTH", "ALA", "CSL", "CPP"]
    create_members(import_chat_id, "Wiz Panic", team_members)

    team_members = ["JJC", "CKR", "DVD", "REI", "XSM", "SSD", "JMN", "CCM"]
    create_members(import_chat_id, "Los Manquificos", team_members)

    team_members = ["JAR", "JRM", "ENR", "GFB", "CJC", "ADN", "SBS", "XPK"]
    create_members(import_chat_id, "Komando kemando el mando", team_members)
